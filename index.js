var MongoClient = require('mongodb').MongoClient;
var config = require('config');
var converter = require('json2csv');
var fs = require('fs');
var db = config.get('dbUrl');
var filePath = config.get('filePath');
MongoClient.connect(db, function (err, dbConn) {
	if (!err) {
        console.log('db connection successfully established');
		var clients = dbConn.collection('clients');
		clients.aggregate([
			{
				$match: { '$and':[{'profileId': { '$ne': null }},{'profileId': { '$ne': 'null'}},{'profileId':{'$ne':'0'}}] }
			},
			{
				$group: { _id: '$profileId', firstArrived: { '$min': "$createdTime" }, lastSeen: { '$max': '$lastSeenTime' } }
			}
		]).toArray(function (err, docs) {
			if(err){
				console.log('got an error fetching data from mongodb');
			}
			console.log('fetched data from mongo db');
			var fields = ['_id','firstArrived','lastSeen'];
			converter({ data: docs, fields: fields }, function (err, csv) {
				if (err) console.log('******got an error*******',err);
				var now = new Date();
				var month = now.getMonth()+1;
				var day = now.getDate();
				var path = filePath+'clientReport-'+now.getFullYear()+'-'+month+'-'+day+'.csv';
				fs.writeFile(path,csv,function(err){
					if(err){
						console.log('got an error saving a csv file',err);
					}
					console.log("done creating a csv file");
				});
				dbConn.close();
			});	
			
		});

	}else{
		console.log('db connection error',err);
	};
});